# Gigasnake

Snake game made with Allegro5, for data structures class.

(Tested for Ubuntu 16.04 and some version of Manjaro)

Demo: https://www.youtube.com/watch?v=v3Oo8VOmTbE

### How to install
1. Install Allegro5
```
sudo add-apt-repository ppa:allegro/5.2
sudo apt update
sudo apt install liballegro5-dev 
```
2. Clone & Compile
```
git clone https://gitlab.com/gustavoflw/gigasnake.git
cd gigasnake/
make
```
3. Run
```
./gigasnake
```

### Controls
Move: W A S D
Debug: F3
Pause: ESC
