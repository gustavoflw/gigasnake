#ifndef GUARD_BORING_H
#define GUARD_BORING_H

#include <stdio.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_native_dialog.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include <allegro5/allegro_primitives.h>
#include <math.h>

#include <input.h>

#define SCR_W 800
#define SCR_H 600

typedef struct font {
    ALLEGRO_FONT *font6;
    ALLEGRO_FONT *font12;
    ALLEGRO_FONT *font18;
    ALLEGRO_FONT *font24;
    ALLEGRO_FONT *font40;
} Font;

typedef struct color {
    ALLEGRO_COLOR red;
    ALLEGRO_COLOR white;
    ALLEGRO_COLOR brightblue;
    ALLEGRO_COLOR darkblue;
    ALLEGRO_COLOR black;
    ALLEGRO_COLOR grey;
    ALLEGRO_COLOR yellow;
} Color;

typedef struct hotkey {
    int up;
    int right;
    int down;
    int left;
    int debug;
    int pause;
} Hotkey;

typedef struct action {   
   bool debug; 
   bool draw;
   bool quit;
   bool click;
   bool pause;
   bool gameover;
   bool shader;
} Action;

typedef struct cursor {
    int x, y;
} Cursor;

void initialise(int FPS, ALLEGRO_DISPLAY **pwindow, ALLEGRO_EVENT_QUEUE **pevent_queue, ALLEGRO_TIMER **ptimer, Font *font);
void startColors(Color *color);
void startHotkeys(Hotkey *hotkey);
void startActions(Action *action);

#endif