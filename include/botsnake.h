#ifndef GUARD_BOTSNAKE_H
#define GUARD_BOTSNAKE_H

#include <snake.h>

Snake *botSnakeInitialize(int x, int y, int elementR, int nElements);
void drawBotSnake(Snake *snake);
void drawBotElement(SnakeElement *element);

bool willHeadCollideWithSnake(Snake* snake, SnakeElement* head, SnakeList* snakeList);
void botSnakeUpdatePosition(Snake *snake, SnakeElement *food, SnakeList *snakeList);
void botSnakeElementUpdateSpeed(SnakeElement *element);

#endif