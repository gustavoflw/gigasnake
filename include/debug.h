#ifndef GUARD_DEBUG_H
#define GUARD_DEBUG_H

#include <snake.h>

void drawDebugSnake(Snake *snake, ALLEGRO_FONT *font, Color color);

#endif