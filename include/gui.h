#ifndef GUARD_GUI_H
#define GUARD_GUI_H

#include <allegro5/allegro.h>
#include <allegro5/allegro_native_dialog.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include <allegro5/allegro_primitives.h>
#include <string.h>

#include <input.h>

typedef struct button {
    int x, y;
    int w, h;
    ALLEGRO_COLOR color;
    ALLEGRO_COLOR idle;
    ALLEGRO_COLOR hover;
    ALLEGRO_COLOR fontc;
    ALLEGRO_FONT *font;
} Button;

void buttonInitialize(Button *button, int x, int y, int w, int h, ALLEGRO_COLOR idlecolor, ALLEGRO_COLOR hovercolor, ALLEGRO_COLOR fontcolor, 
ALLEGRO_FONT *font);
bool cursorInButton(Cursor cursor, Button *button);
void updateButtonColor(Cursor cursor, Button *button);
void drawButton(Button *button, const char *text);

#endif