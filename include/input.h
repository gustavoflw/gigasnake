#ifndef GUARD_INPUT_H
#define GUARD_INPUT_H

#include <stdio.h>
#include <allegro5/allegro.h>
#include <allegro5/events.h>

#include <boring.h>

typedef struct action Action;
typedef struct hotkey Hotkey;

void read_input(Action *action, ALLEGRO_EVENT event, bool *keys, Hotkey hotkey);

#endif