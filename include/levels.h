#ifndef GUARD_LEVELS_H
#define GUARD_LEVELS_H

#include <allegro5/allegro_opengl.h>

#include <snake.h>
#include <playersnake.h>
#include <botsnake.h>
#include <debug.h>
#include <gui.h>
#include <shaders.h>
#include <stack.h>
#include <snakelist.h>
#include <ouroboros.h>

#define MENU 0
#define GAME 1
#define OPTIONS 2
#define TESTS 3

void menu(int *level, Action *action, Hotkey *hotkey, Color *color, Font *font, bool *keys, ALLEGRO_EVENT_QUEUE *event_queue, 
    ALLEGRO_DISPLAY *display);
void game(int *level, Action *action, Hotkey *hotkey, Color *color, Font *font, float *speed, bool *keys, ALLEGRO_EVENT_QUEUE *event_queue, 
    ALLEGRO_DISPLAY *display);
void tests(int *level, Action *action);

#endif