#ifndef GUARD_MATHS_H
#define GUARD_MATHS_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>

int circleCenter_x(int x, int r);
int circleCenter_y(int y, int r);
int is_in_radius(float x, float y, float x0, float y0, float R);
int doCirclesIntersect(float x0, float y0, int R0, float x1, float y1, int R1);
int randRange(int lower, int upper);
bool inCircle(float x, float y, float x0, float y0, float r);
void linearizeVector(float *vx, float *vy);
float getVectorsAngle(float ux, float uy, float vx, float vy);
float distanceAB(int Ax, int Ay, int Bx, int By);

#endif