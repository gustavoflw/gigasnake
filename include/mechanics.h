#ifndef GUARD_MECHANICS_H
#define GUARD_MECHANICS_H

#include <stdio.h>
#include <stdlib.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>

#include <boring.h>
#include <maths.h>
#include <snake.h>

typedef struct snakeElement SnakeElement;
typedef struct snake Snake;
typedef struct color Color;

int collision(SnakeElement *A, SnakeElement *B);
void eatFood(Snake *snake, SnakeElement *food);

#endif