#ifndef GUARD_OUROBOROS_H
#define GUARD_OUROBOROS_H

#include <snake.h>

void ouroborosUpdatePosition(Snake *snake, int Cx, int Cy, int r);
void ouroborosUpdateSpeed(SnakeElement *element, int Cx, int Cy, int r);

// Snake *ouroborosInitialize(int elementR, int nElements, int x, int y);
// void ouroborosUpdatePosition(Snake *snake, int Cx, int Cy, int r);
// void ouroborosElementUpdateSpeed(SnakeElement *element, int Cx, int Cy, int r);

#endif