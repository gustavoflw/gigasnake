#ifndef GUARD_PLAYERSNAKE_H
#define GUARD_PLAYERSNAKE_H

#include <snake.h>

Snake *playerSnakeInitialize(int x, int y, int elementR, int nElements, ALLEGRO_COLOR color);
void playerSnakeUpdatePosition(bool *keys, Snake *snake, float speed, SnakeElement *B, Hotkey hotkey);
int playerElementUpdateSpeed(bool *keys, SnakeElement *element, float speed, Hotkey hotkey);

#endif