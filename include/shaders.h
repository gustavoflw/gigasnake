#ifndef GUARD_SHADERS_H
#define GUARD_SHADERS_H

#include <snake.h>

void startShaders(ALLEGRO_SHADER *shader);
void drawRadientShader(float radius, int Cx, int Cy, float expand, float alpha, ALLEGRO_COLOR color);

#endif