#ifndef GUARD_SNAKE_H
#define GUARD_SNAKE_H

#include <stdio.h>
#include <stdlib.h>
#include <allegro5/allegro.h>
#include <allegro5/events.h>

#include <mechanics.h>
#include <boring.h>
#include <input.h>
#include <math.h>
#include <maths.h>
#include <stack.h>

#define DEFRADIUS SCR_W*1875/100000
#define BOT 0
#define PLAYER 1
#define BACKGROUND 2

typedef struct color Color;

typedef struct snakeElement {
    float *position;
    float *v;
    int radius;
    ALLEGRO_COLOR color;
    struct snakeElement *next;
    struct snakeElement *before;
} SnakeElement;

typedef struct snake {
    SnakeElement *first;
    SnakeElement *last;
    bool player;
    Pilha *px;
    Pilha *py;
} Snake;

typedef struct snakeListElement {
    Snake *snake;
    struct snakeListElement *next;
} SnakeListElement;

typedef struct snakeList {
    SnakeListElement *first;
} SnakeList;

Snake *snakeInitialize();
SnakeElement *snakeElementInitialize(float radius);
int snakeLength(Snake *snake);
int snakeInsert(Snake *snake, int n, SnakeElement *element, float *elementStartPosition, ALLEGRO_COLOR *color);
void snakeElementClear(SnakeElement *element);
void snakeClear(Snake *snake);

void drawSnake(Snake *snake);
void drawSnakeElement(SnakeElement *element);

#endif