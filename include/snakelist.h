#ifndef GUARD_SNAKELIST_H
#define GUARD_SNAKELIST_H

#include <snake.h>

SnakeList *snakeListInitialize();
SnakeListElement *snakeListElementInitialize();
int snakeListLength(SnakeList *snakeList);
void snakeListInsert(SnakeList *snakeList, Snake *snake);
void snakeListElementClear(SnakeListElement* iterator);
void snakeListClear(SnakeList* snakeList);

#endif