#ifndef GUARD_STACK_H
#define GUARD_STACK_H

#include <stdio.h>

#include <stdio.h>
#include <stdlib.h>

typedef struct elementoPilha
{
	int dado;
	struct elementoPilha *proximo;
} ElementoPilha;

typedef struct pilha 
{
    ElementoPilha *topo;
} Pilha;

Pilha *inicializaPilha() ;

void Push(Pilha *p, int d);

int Pop(Pilha *p);

void imprimePilha(Pilha *p) ;

void liberarPilha (Pilha *p);

#endif