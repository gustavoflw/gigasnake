uniform vec4 color;
uniform float expand;
uniform vec2 center;
uniform float radius;
uniform float windowHeight;
void main(void)
{
	vec2 centerCorrected = vec2(center.x, windowHeight - center.y);
    vec2 p = (gl_FragCoord.xy - centerCorrected) / radius; // vetor do pixel do centro do circulo até o fragmento (glFragCoord.xy == coord. do fragmnt.)
	float r = sqrt(dot(p, p)); // r é o módulo de p (raio)
	if (r < 1.0) { // se o fragmento estiver dentro do raio...
		gl_FragColor = mix(color, gl_Color, (r - expand) / (1.0 - expand)); // mistura a cor (depende do valor de expansão)
	}
	else { // se não, a cor do pixel permanece igual à cor do fragmento
		gl_FragColor = gl_Color;
	}
    // gl_FragColor = gl_Color; // descomentar isso p/ que o shader de fragmento nao faça nada
};