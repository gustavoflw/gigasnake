void main()
{
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex; // posição do espaço do gl sera a posiçao do espaço do vertex vezes a matriz de projeção
	gl_TexCoord[0] = gl_TextureMatrix[0] * gl_MultiTexCoord0; // seta a coordenada de textura real a partir do que o allegro passa 
	gl_FrontColor = gl_Color; // cor da frente do triangulo é a cor q foi setada
};