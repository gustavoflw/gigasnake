#include <boring.h>

void initialise(int FPS, ALLEGRO_DISPLAY **pwindow, ALLEGRO_EVENT_QUEUE **pevent_queue, ALLEGRO_TIMER **ptimer, Font *font)
{   
    //initalise allegro and its addons
    al_init();
    al_init_image_addon();
    al_install_audio();
    al_init_acodec_addon();
    al_init_font_addon();
    al_init_ttf_addon(); 
    al_install_keyboard();
    al_install_mouse();
    al_init_primitives_addon();
    //event queue
    *pevent_queue = al_create_event_queue();
    //window
    // al_set_new_display_flags(ALLEGRO_FULLSCREEN_WINDOW);
    *pwindow = al_create_display(SCR_W, SCR_H);
    al_set_window_title(*pwindow, "Gigasnake");
    al_clear_to_color(al_map_rgb(50,50,50));
    al_flip_display();
    //cursor
    al_set_system_mouse_cursor(*pwindow, ALLEGRO_SYSTEM_MOUSE_CURSOR_DEFAULT);
    //fonts
    font->font6 = al_load_font("./resources/arial.ttf", 6, 0);
    if (font->font6 == NULL)
        puts("cant load font");

    font->font12 = al_load_font("./resources/arial.ttf", 12, 0);
    if (font->font12 == NULL)
        puts("cant load font");

    font->font18 = al_load_font("./resources/arial.ttf", 18, 0);
    if (font->font18 == NULL)
        puts("cant load font");

    font->font24 = al_load_font("./resources/arial.ttf", 24, 0);
    if (font->font24 == NULL)
        puts("cant load font");
    
    font->font40 = al_load_font("./resources/arial.ttf", 40, 0);
    if (font->font40 == NULL)
        puts("cant load font");
    
    //timer
    *ptimer = al_create_timer(1.0/FPS);
    al_start_timer(*ptimer);
    //registers event sources
    al_register_event_source(*pevent_queue, al_get_mouse_event_source());
    al_register_event_source(*pevent_queue, al_get_keyboard_event_source());
    al_register_event_source(*pevent_queue, al_get_display_event_source(*pwindow));
    al_register_event_source(*pevent_queue, al_get_timer_event_source(*ptimer));
}

void startColors(Color *color)
{
    color->red = al_map_rgb(255,0,0);    
    color->white = al_map_rgb(255,255,255);
    color->brightblue = al_map_rgb(0,150,255);
    color->darkblue = al_map_rgb(0,0,255);
    color->black = al_map_rgb(0,0,0);
    color->grey = al_map_rgb(80,80,80);
    color->yellow = al_map_rgb(255,255,0);
}

void startHotkeys(Hotkey *hotkey)
{
    hotkey->up = ALLEGRO_KEY_W;
    hotkey->right = ALLEGRO_KEY_D;
    hotkey->down = ALLEGRO_KEY_S;
    hotkey->left = ALLEGRO_KEY_A;
    hotkey->debug = ALLEGRO_KEY_F3;
    hotkey->pause = ALLEGRO_KEY_ESCAPE;
}

void startActions(Action *action)
{
    action->debug = 0;
    action->draw = 0;
    action->quit = 0;
    action->click = 0;
    action->pause = 0;
    action->gameover = 0;
    action->shader = 0;
}