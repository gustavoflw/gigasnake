#include <botsnake.h>

Snake *botSnakeInitialize(int x, int y, int elementR, int nElements)
{
    SnakeElement *element = snakeElementInitialize(elementR);
    element->position[0] = x;
    element->position[1] = y;
    element->color = al_map_rgb(randRange(100,255), randRange(100,255), randRange(100,255));
    Snake *snake = snakeInitialize();
    snake->px = inicializaPilha();
    snake->py = inicializaPilha();
    snakeInsert(snake, nElements, element, element->position, &element->color);
    return snake;
}

void drawBotSnake(Snake *snake) {
    // for (int x=0; x <= SCR_W; x = x + 100) {
    //     for (int y=0; y <= SCR_H; y = y + 100) {
    //         al_draw_pixel(x, y, color.grey);
    //         al_draw_filled_circle(x, y, DEFRADIUS*3, color.grey);
    //     }
    // }

    SnakeElement *iterator = snake->last;
    while (iterator != NULL) {
        drawBotElement(iterator);
        iterator = iterator->before; 
    }
}

void drawBotElement(SnakeElement *element)
{
    float R = element->radius;
    float cx = circleCenter_x(element->position[0], R);
    float cy = circleCenter_y(element->position[1], R);

    al_draw_filled_circle(cx, cy, R, element->color);
    // al_draw_circle(cx, cy, R, al_map_rgb(0,0,0), 5);
}

bool willHeadCollideWithSnake(Snake* snake, SnakeElement* head, SnakeList* snakeList)
{
    bool collide = 0;
    
    SnakeListElement* listIterator = snakeList->first;
    while (listIterator != NULL) {
        if (snake != listIterator->snake) {
            SnakeElement* elementIterator = listIterator->snake->first; //
            while (elementIterator != NULL) {
                if (collision(head, elementIterator))
                    collide = 1;
                if (collide)
                    break;
                elementIterator = elementIterator->next;
            }
            if (collide)
                break;
        }
        listIterator = listIterator->next;
    }

    return collide;
}

void botSnakeUpdatePosition(Snake *snake, SnakeElement *food, SnakeList *snakeList)
{
    int x0 = (int)snake->first->position[0]+snake->first->radius, y0 = (int)snake->first->position[1]+snake->first->radius;
    float vx = 0, vy = 0;

    if (snake->px->topo == NULL)
        for (int i=0; i<10; i++)
            Push(snake->px, randRange(0, SCR_W));
    if (snake->py->topo == NULL)
        for (int i=0; i<10; i++)
            Push(snake->py, randRange(0, SCR_H));

    // If near food, prioritizes moving towards it
    if (is_in_radius(x0, y0, food->position[0]+food->radius, food->position[1]+food->radius, food->radius*3)) {
        if (!is_in_radius(x0, y0, food->position[0]+food->radius, food->position[1]+food->radius, food->radius*1)) {
            vx = (float)food->position[0]+food->radius - x0;
            vy = (float)food->position[1]+food->radius - y0;
            linearizeVector(&vx, &vy);
        }
    }
    // If not near food, and close to the next position in the stack, removes coords from stack
    else if (is_in_radius(x0, y0, snake->px->topo->dado, snake->py->topo->dado, 10)) {
        vx = (float)Pop(snake->px);
        vy = (float)Pop(snake->py);;
        linearizeVector(&vx, &vy);
    }
    // If not near food, and not close to stack, will move towards next coords in stack
    else {
        vx = (float)snake->px->topo->dado - x0;
        vy = (float)snake->py->topo->dado - y0;
        linearizeVector(&vx, &vy);      
    }

    snake->first->v[0] = vx;
    snake->first->v[1] = vy;
    snake->first->position[0] = snake->first->position[0] + snake->first->v[0];
    snake->first->position[1] = snake->first->position[1] + snake->first->v[1];

    if (collision(snake->first, food)) {
        eatFood(snake, food); 
    }

    // int count = 0;
    // int start = 0;
    // while (willHeadCollideWithSnake(snake, snake->first, snakeList)) {
    //     // if (count > 200)
    //         // break;
    //     // count++;

    //     x0 = (int)snake->first->position[0]+snake->first->radius;
    //     y0 = (int)snake->first->position[1]+snake->first->radius;

    //     if (!start) {
    //         while (snake->px->topo || snake->py->topo) {
    //             Pop(snake->px);
    //             Pop(snake->py);
    //         }
    //     }

    //     if (snake->px->topo == NULL)
    //         Push(snake->px, randRange(0, SCR_W));
    //     if (snake->py->topo == NULL)
    //         Push(snake->py, randRange(0, SCR_H));
    //     if (is_in_radius(x0, y0, snake->px->topo->dado, snake->py->topo->dado, 10)) {
    //         vx = (float)Pop(snake->px) - x0;
    //         vy = (float)Pop(snake->py) - y0;
    //         linearizeVector(&vx, &vy);
    //     }
    //     else {
    //         vx = (float)snake->px->topo->dado - x0;
    //         vy = (float)snake->py->topo->dado - y0;
    //         linearizeVector(&vx, &vy);      
    //     }

    //     snake->first->v[0] = vx;
    //     snake->first->v[1] = vy;
    //     snake->first->position[0] = snake->first->position[0] + snake->first->v[0];
    //     snake->first->position[1] = snake->first->position[1] + snake->first->v[1];
    // }

    SnakeElement* iterator = snake->last;
    while (iterator->before != NULL) {
        botSnakeElementUpdateSpeed(iterator);
        iterator->position[0] = iterator->position[0] + iterator->v[0];
        iterator->position[1] = iterator->position[1] + iterator->v[1];
        iterator = iterator->before;
    }
}

void botSnakeElementUpdateSpeed(SnakeElement *element)
{
    if (element->before != NULL) {
        // element->v[0] = 0.05*(element->before->position[0] - element->position[0]);
        // element->v[1] = 0.05*(element->before->position[1] - element->position[1]);

        float d = distanceAB(element->before->position[0]+element->before->radius, element->before->position[1]+element->before->radius, 
                            element->position[0]+element->radius, element->position[1]+element->radius);
        float mult = 0;
        if (!is_in_radius(
            element->before->position[0]+element->before->radius, element->before->position[1]+element->before->radius, 
            element->position[0]+element->radius, element->position[1]+element->radius, element->before->radius
            )
        )
            mult = element->before->radius*0.14 / d;
        else
            mult = 0.035;
        element->v[0] = mult*(element->before->position[0]+element->before->radius - (element->position[0]+element->radius));
        element->v[1] = mult*(element->before->position[1]+element->before->radius - (element->position[1]+element->radius));
    }
}