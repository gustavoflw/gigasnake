#include <debug.h>

void drawDebugSnake(Snake *snake, ALLEGRO_FONT *font, Color color)
{
    int y = 0, i = 0;

    al_draw_textf(font, color.white, snake->first->position[0]+30, snake->first->position[1]+30, ALLEGRO_ALIGN_LEFT, "%.0f, %.0f", snake->first->position[0], snake->first->position[1]);
    al_draw_textf(font, color.white, snake->first->position[0]+30, snake->first->position[1]+40, ALLEGRO_ALIGN_LEFT, "%.0f, %.0f", snake->first->v[0], snake->first->v[1]);

    if (snake->px && snake->py && snake->px->topo && snake->py->topo) {
        al_draw_filled_circle(snake->px->topo->dado, snake->py->topo->dado, 5, snake->first->color);
        al_draw_line(snake->first->position[0]+snake->first->radius, snake->first->position[1]+snake->first->radius, 
            snake->px->topo->dado, snake->py->topo->dado, snake->first->color, 1);
    }

    SnakeElement *iterator = snake->first;
    while (iterator != NULL) {

        float R = iterator->radius;
        float x0 = iterator->position[0];
        float y0 = iterator->position[1];
        float vx = iterator->v[0];
        float vy = iterator->v[1];
        float w = 2*R;
        float h = 2*R;
        float cx = circleCenter_x(x0, R);
        float cy = circleCenter_y(y0, R);

        al_draw_filled_circle(cx, cy, 6, color.red);
        al_draw_rectangle(x0, y0, x0 + w, y0 + h, color.white, 1);
        al_draw_line(cx, cy, cx + 50*vx, cy + 50*vy, color.yellow, 1);
        
        // al_draw_textf(font, color.white, 0.75*SCR_W, y, ALLEGRO_ALIGN_LEFT, "snake[%i] %.0f, %.0f", i, iterator->position[0], iterator->position[1]);

        i = i + 1;
        y = y + 12;
        iterator = iterator->next;
    }
}