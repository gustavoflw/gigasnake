#include <gui.h>

void buttonInitialize(Button *button, int x, int y, int w, int h, ALLEGRO_COLOR idlecolor, ALLEGRO_COLOR hovercolor, ALLEGRO_COLOR fontcolor, 
ALLEGRO_FONT *font)
{
    button->x = x;
    button->y = y;
    button->w = w;
    button->h = h;
    button->color = idlecolor;
    button->idle = idlecolor;
    button->hover = hovercolor;
    button->fontc = fontcolor;
    button->font = font;
}

bool cursorInButton(Cursor cursor, Button *button)
{
    if (cursor.x >= button->x && cursor.x <= button->x+button->w && cursor.y >= button->y && cursor.y <= button->y+button->h)
        return 1;
    return 0;
}

void updateButtonColor(Cursor cursor, Button *button)
{
    if (cursorInButton(cursor, button)) {
        button->color = button->hover;
    }
    else {
        button->color = button->idle;
    }
}

void drawButton(Button *button, const char *text)
{   
    al_draw_filled_rounded_rectangle(button->x, button->y, button->x + button->w, button->y + button->h, 0.5, 0.5, button->color);
    al_draw_rounded_rectangle(button->x, button->y, button->x + button->w, button->y + button->h, 0.5, 0.5, al_map_rgb(0,0,0), 5);
    al_draw_text(button->font, button->fontc, button->x+button->w/2, button->y, ALLEGRO_ALIGN_CENTER, text);
}