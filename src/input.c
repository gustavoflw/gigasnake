#include <input.h>

void read_input(Action *action, ALLEGRO_EVENT event, bool *keys, Hotkey hotkey)
{
    if (event.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
        action->quit = 1;
        puts("action.quit");
    }
    else if (event.type == ALLEGRO_EVENT_KEY_DOWN){
        keys[event.keyboard.keycode] = true;
        puts("key down");

        if (event.keyboard.keycode == hotkey.debug) {
            if (action->debug == 0)
                action->debug = 1;
            else
                action->debug = 0;
            
        }
        else if (event.keyboard.keycode == hotkey.pause) {
            if (action->pause == 0)
                action->pause = 1;
            else
                action->pause = 0;
        }
    }
    else if (event.type == ALLEGRO_EVENT_KEY_UP) {
        keys[event.keyboard.keycode] = false;
        puts("key up");
    }
    else if (event.type == ALLEGRO_EVENT_KEY_CHAR) {
        puts("key char");
    }
    else if (event.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN){
        puts("mouse btn down");
        action->click = 1;
    }
}