#include <levels.h>

void menu(int *level, Action *action, Hotkey *hotkey, Color *color, Font *font, bool *keys, ALLEGRO_EVENT_QUEUE *event_queue, 
    ALLEGRO_DISPLAY *display)
{  
    ALLEGRO_COLOR background = color->black;
    Cursor cursor = {0,0};
    Button start, quit, shaderOnOff;
    ALLEGRO_BITMAP *buffer = al_create_bitmap(SCR_W, SCR_H);
    ALLEGRO_TIMER *timer2 = al_create_timer(0.01);
    ALLEGRO_AUDIO_STREAM *music = NULL;

    ALLEGRO_SHADER *shader = al_create_shader(ALLEGRO_SHADER_GLSL);
    startShaders(shader);

    al_reserve_samples(1);
    music = al_load_audio_stream("resources/cyberpunk.ogg", 4, 1024);
    if (!music)
        puts("music error");
    al_attach_audio_stream_to_mixer(music, al_get_default_mixer());
    al_set_audio_stream_gain(music, 0.15);
    al_set_audio_stream_playing(music, true);

    buttonInitialize(&start, SCR_W*6/8, SCR_H*4/16, 140, 60, color->yellow, color->white, color->black, font->font24);
    buttonInitialize(&quit, SCR_W*6/8, SCR_H*12/16, 140, 60, color->red, color->white, color->black, font->font24);
    buttonInitialize(&shaderOnOff, SCR_W*6/8, SCR_H*8/16, 140, 60, color->brightblue, color->white, color->black, font->font18);

    al_register_event_source(event_queue, al_get_timer_event_source(timer2));
    al_start_timer(timer2);

    // Snake List
    SnakeList *snakeList = snakeListInitialize();
    // BOTs
    int len = 15;
    Snake **bots = (Snake**)malloc(sizeof(Snake)*len);
    for (int i=0; i<len; i++) {
        bots[i] = botSnakeInitialize(randRange(0,SCR_W), randRange(0,SCR_H), randRange(DEFRADIUS-7,DEFRADIUS-3), 17);
        snakeListInsert(snakeList, bots[i]);
    }
    free(bots);

    while (action->quit == 0 && *level == MENU) {
        ALLEGRO_EVENT event;
        al_wait_for_event(event_queue, &event);
        read_input(action, event, keys, *hotkey);
        if (event.type == ALLEGRO_EVENT_TIMER) {
            if (event.timer.source == timer2) {
                if (action->shader) {
                    al_set_blender(ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_ONE);
                    al_set_target_bitmap(buffer);
                    al_clear_to_color(al_map_rgba(0,0,0,0));
                
                    al_use_shader(shader);
                    drawRadientShader(DEFRADIUS*50, SCR_W/2, SCR_H/2, 0.f, 0.9, al_map_rgb(100,100,100));
                    SnakeElement *snakeIterator = NULL;
                    SnakeListElement *listIterator = snakeList->first;
                    while (listIterator != NULL) {
                        snakeIterator = listIterator->snake->last;
                        while (snakeIterator != NULL) {
                            drawRadientShader(snakeIterator->radius*3, snakeIterator->position[0]+snakeIterator->radius, 
                                            snakeIterator->position[1]+snakeIterator->radius, 0.f, 0.6, snakeIterator->color);
                            snakeIterator = snakeIterator->before;
                        }
                        listIterator = listIterator->next;
                    }
                    al_use_shader(NULL);
                    al_set_blender(ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA);
                    al_set_target_backbuffer(display);
                }
                else {
                    al_set_target_bitmap(buffer);
                    al_clear_to_color(al_map_rgba(0,0,0,0));
                    al_set_target_backbuffer(display);
                }
            }
            else {
                SnakeListElement *iterator = snakeList->first;
                int R = 90;
                while (iterator != NULL) {
                    ouroborosUpdatePosition(iterator->snake, SCR_W/2, SCR_H/2, R);
                    R += 30;
                    iterator = iterator->next;
                }
                action->draw = 1;
            }
        }
        if (action->click) {
            if (cursorInButton(cursor, &start))
                *level = GAME;
            else if (cursorInButton(cursor, &quit))
                action->quit = 1;
            else if (cursorInButton(cursor, &shaderOnOff)) {
                switch(action->shader) {
                    case 0:
                        action->shader = 1;
                        break;
                    case 1:
                        action->shader = 0;
                        break;
                }
            }
            action->click = 0;
        }
        if (event.type == ALLEGRO_EVENT_MOUSE_AXES) {
            cursor.x = event.mouse.x;
            cursor.y = event.mouse.y;
            updateButtonColor(cursor, &start);
            updateButtonColor(cursor, &quit);
            updateButtonColor(cursor, &shaderOnOff);
        }
        if (action->draw && al_is_event_queue_empty(event_queue)) {
            al_clear_to_color(background);
            al_draw_bitmap(buffer, 0, 0, 0);
            SnakeListElement *iterator = snakeList->first;
            while (iterator != NULL) {
                drawBotSnake(iterator->snake);
                iterator = iterator->next;
            }
            drawButton(&start, "Start");
            drawButton(&quit, "Quit");
            drawButton(&shaderOnOff, "Shader ON/OFF");
            if (action->debug) {
                al_draw_line(SCR_W/2, 0, SCR_W/2, SCR_H, color->white, 1);
                al_draw_line(0, SCR_H/2, SCR_W, SCR_H/2, color->white, 1);
                SnakeListElement *iterator = snakeList->first;
                while (iterator != NULL) {
                    drawDebugSnake(iterator->snake, font->font12, *color);
                    iterator = iterator->next;
                }
            }
            al_draw_textf(font->font24, color->white, SCR_W/2, 0, ALLEGRO_ALIGN_CENTRE, "Welcome to Gigasnake");
            al_flip_display();
            action->draw = 0;
        }        
    }
    snakeListClear(snakeList);
    al_destroy_shader(shader);
    al_destroy_bitmap(buffer);
    al_destroy_timer(timer2);
    al_destroy_audio_stream(music);
}

void game(int *level, Action *action, Hotkey *hotkey, Color *color, Font *font, float *speed, bool *keys, ALLEGRO_EVENT_QUEUE *event_queue, 
    ALLEGRO_DISPLAY *display) 
{
    al_clear_to_color(color->black);
    ALLEGRO_COLOR background = color->black;
    ALLEGRO_BITMAP *buffer = al_create_bitmap(SCR_W, SCR_H);
    ALLEGRO_BITMAP *head = al_load_bitmap("resources/emoji.png");
    ALLEGRO_TIMER *timer2 = al_create_timer(0.01);
    ALLEGRO_AUDIO_STREAM *music = NULL;
    Cursor cursor = {0,0};
    Button back2menu, unpause;

    buttonInitialize(&back2menu, SCR_W*1/2-150, SCR_H*1/2+45, 300, 30, color->grey, color->white, color->brightblue, font->font24);
    buttonInitialize(&unpause, SCR_W*1/2-150, SCR_H*1/2-25, 300, 50, color->grey, color->white, color->brightblue, font->font40);

    al_reserve_samples(1);
    music = al_load_audio_stream("resources/doom.ogg", 4, 1024);
    al_set_audio_stream_gain(music, 0.0015);
    if (!music)
        puts("music error");
    al_attach_audio_stream_to_mixer(music, al_get_default_mixer());
    al_set_audio_stream_playing(music, true);

    al_register_event_source(event_queue, al_get_timer_event_source(timer2));
    al_start_timer(timer2);
    
    ALLEGRO_SHADER *shader = al_create_shader(ALLEGRO_SHADER_GLSL);
    startShaders(shader);
    
    // Snake list
    SnakeList *snakeList = snakeListInitialize();
    // Snake
    Snake *snake = playerSnakeInitialize(SCR_W/2, SCR_H/2, DEFRADIUS, 3, al_map_rgb(0,255,0));
    snakeListInsert(snakeList, snake);
    // Food
    SnakeElement *food = snakeElementInitialize(DEFRADIUS);
    food->position[0] = 100;
    food->position[1] = 100;
    food->color = color->yellow;
    // BOTs
    int botsSize = 7;
    Snake **bots = (Snake**)malloc(sizeof(Snake)*botsSize);
    for (int i=0; i<botsSize; i++) {
        bots[i] = botSnakeInitialize(randRange(0,SCR_W), randRange(0,SCR_H), randRange(DEFRADIUS-6,DEFRADIUS), 5);
        snakeListInsert(snakeList, bots[i]);
    }
    free(bots);

    while (action->quit == 0 && *level == GAME) {
        ALLEGRO_EVENT event;
        al_wait_for_event(event_queue, &event);
        read_input(action, event, keys, *hotkey);

        if (!action->pause && !action->gameover) {
            if (event.type == ALLEGRO_EVENT_TIMER) {
                if (event.timer.source == timer2) {
                    if (action->shader) {
                        al_set_blender(ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_ONE);
                        al_set_target_bitmap(buffer);
                        al_clear_to_color(al_map_rgba(0,0,0,0));
                        al_use_shader(shader);
                        drawRadientShader(food->radius*1, food->position[0]+food->radius, food->position[1]+food->radius, 0.25f, 1.0, food->color);
                        drawRadientShader(food->radius*15, food->position[0]+food->radius, food->position[1]+food->radius, 0.f, 1.0, food->color);
                        SnakeElement *snakeIterator = snake->last;
                        while (snakeIterator != NULL) {
                            drawRadientShader(snakeIterator->radius*2, snakeIterator->position[0]+snakeIterator->radius, snakeIterator->position[1]+snakeIterator->radius, 0.1f, 0.4, snakeIterator->color);
                            snakeIterator = snakeIterator->before;
                        }
                        SnakeListElement *listIterator = snakeList->first;
                        while (listIterator != NULL) {
                            SnakeElement* snakeIterator = listIterator->snake->last;
                            while (snakeIterator != NULL) {
                                drawRadientShader(snakeIterator->radius*3, snakeIterator->position[0]+snakeIterator->radius, 
                                                snakeIterator->position[1]+snakeIterator->radius, 0.1f, 0.4, snakeIterator->color);
                                snakeIterator = snakeIterator->before;
                            }
                            listIterator = listIterator->next;
                        }
                        al_set_blender(ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA); // default blender
                        al_use_shader(NULL);
                        al_set_target_backbuffer(display);
                    }
                    else {
                        al_set_target_bitmap(buffer);
                        al_clear_to_color(al_map_rgba(0,0,0,0));
                        al_set_target_backbuffer(display);
                    }
                }
                else {
                    playerSnakeUpdatePosition(keys, snake, *speed, food, *hotkey);
                    SnakeListElement *iterator = snakeList->first->next;
                    while (iterator != NULL) {
                        botSnakeUpdatePosition(iterator->snake, food, snakeList);
                        iterator = iterator->next;
                    }
                    if (snake->first->position[0]+snake->first->radius > SCR_W || snake->first->position[0]+snake->first->radius < 0 || 
                        snake->first->position[1]+snake->first->radius > SCR_H || snake->first->position[1]+snake->first->radius < 0)
                        action->gameover = 1;
                    action->draw = 1;
                }
            }
            if (action->draw && al_is_event_queue_empty(event_queue)) {
                al_clear_to_color(background);
                al_draw_bitmap(buffer, 0, 0, 0);
                if (!action->shader)
                    drawSnakeElement(food);
                drawSnake(snake);
                al_draw_scaled_bitmap(head, 0, 0, al_get_bitmap_width(head), al_get_bitmap_height(head), 
                    snake->first->position[0], snake->first->position[1], 
                    snake->first->radius*2, snake->first->radius*2, 0);
                SnakeListElement *iterator = snakeList->first->next;
                while (iterator != NULL) {
                    drawBotSnake(iterator->snake);
                    iterator = iterator->next;
                }
                al_draw_textf(font->font24, color->yellow, SCR_W/2, 0, ALLEGRO_ALIGN_CENTRE, "Length: %i", snakeLength(snake));
                if (action->debug) {
                    al_draw_line(SCR_W/2, 0, SCR_W/2, SCR_H, color->white, 1);
                    al_draw_line(0, SCR_H/2, SCR_W, SCR_H/2, color->white, 1);
                    iterator = snakeList->first;
                    while (iterator != NULL) {
                        drawDebugSnake(iterator->snake, font->font12, *color);
                        iterator = iterator->next;
                    }
                }
                al_draw_rectangle(0, 0, SCR_W, SCR_H, color->red, 5);
                al_flip_display();
                action->draw = 0;
            }
        }
        else if (action->pause && !action->gameover) {
            if (event.type == ALLEGRO_EVENT_MOUSE_AXES) {
                cursor.x = event.mouse.x;
                cursor.y = event.mouse.y;
                updateButtonColor(cursor, &unpause);
                updateButtonColor(cursor, &back2menu);
            }
            if (event.type == ALLEGRO_EVENT_TIMER && event.timer.source != timer2) {
                action->draw = 1;
            }
            if (action->click) {
                if (cursorInButton(cursor, &unpause)) {
                    action->pause = 0;
                    action->gameover = 0;
                }
                else if (cursorInButton(cursor, &back2menu)) {
                    *level = MENU;
                    action->pause = 0;
                    action->debug = 0;
                    action->draw = 0;
                    action->gameover = 0;
                }
                action->click = 0;
            }
            if (action->draw) {
                al_clear_to_color(al_map_rgba(0,0,0,0));
                al_use_shader(shader);
                drawRadientShader(500, SCR_W/2, SCR_H/2, 0.f, 0.5, al_map_rgb(100,0,255));
                al_use_shader(NULL);
                drawButton(&unpause, "Continue");
                drawButton(&back2menu, "Quit to menu");
                al_flip_display();
                action->draw = 0;
            }
        }
        else if (action->gameover) {
            if (event.type == ALLEGRO_EVENT_MOUSE_AXES) {
                cursor.x = event.mouse.x;
                cursor.y = event.mouse.y;
                updateButtonColor(cursor, &back2menu);
            }
            if (event.type == ALLEGRO_EVENT_TIMER && event.timer.source != timer2) {
                action->draw = 1;
            }
            if (action->click) {
                if (cursorInButton(cursor, &back2menu)) {
                    *level = MENU;
                    action->pause = 0;
                    action->debug = 0;
                    action->draw = 0;
                    action->gameover = 0;
                }
                action->click = 0;
            }
            if (action->draw) {
                al_clear_to_color(al_map_rgba(0,0,0,0));
                al_draw_text(font->font40, color->red, SCR_W/2, SCR_H/2, ALLEGRO_ALIGN_CENTER, "GAME OVER.");
                al_use_shader(shader);
                drawRadientShader(500, SCR_W/2, SCR_H/2, 0.f, 0.1, al_map_rgb(155,0,0));
                al_use_shader(NULL);
                drawButton(&back2menu, "Quit to menu");
                al_flip_display();
                action->draw = 0;
            }
        }
    }
    snakeListClear(snakeList);
    snakeElementClear(food);
    al_destroy_shader(shader);
    al_destroy_timer(timer2);
    al_destroy_bitmap(head);
    al_destroy_bitmap(buffer);
    al_destroy_audio_stream(music);
    // al_rest(5);
}

void tests(int *level, Action *action)
{
    Pilha *px = inicializaPilha();
    Pilha *py = inicializaPilha();
    for (int i=0; i<10; i++) {
        Push(px, randRange(0, SCR_W));
        Push(py, randRange(0, SCR_H));
    }
    imprimePilha(px);
    imprimePilha(py);
    liberarPilha(px);
    liberarPilha(py);

    while (action->quit == 0 && *level == TESTS) {
               
    }
}