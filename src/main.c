#include <stdio.h>

#include <allegro5/allegro.h>
#include <allegro5/allegro_native_dialog.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include <allegro5/allegro_primitives.h>

#include <boring.h>
#include <snake.h>
#include <snakelist.h>
#include <input.h>
#include <maths.h>
#include <mechanics.h>
#include <playersnake.h>
#include <botsnake.h>
#include <gui.h>
#include <levels.h>
#include <shaders.h>
#include <debug.h>

/* TODO
    - deQueue (com tecla pra largar bolinhas)
    - liberar cobra e elementos
*/

int main()
{   
    ALLEGRO_DISPLAY *display = NULL;
    ALLEGRO_EVENT_QUEUE *event_queue = NULL;
    ALLEGRO_TIMER *timer = NULL;
    Color color;
    Hotkey hotkey;
    Action action;
    Font font;
       
    int FPS = 60;
    float speed = 2;
    action.debug = 0;
    action.draw = 1;
    action.quit = 0;
    bool keys[ALLEGRO_KEY_MAX] = {0};
    int level = MENU;

    // Initialisation
    initialise(FPS, &display, &event_queue, &timer, &font);
    startColors(&color);
    startHotkeys(&hotkey);
    startActions(&action);

    while (action.quit == 0) {
        // puts("switching levels...");
        switch (level) {
            case MENU:
                menu(&level, &action, &hotkey, &color, &font, keys, event_queue, display);
                break;
            case GAME:
                game(&level, &action, &hotkey, &color, &font, &speed, keys, event_queue, display);
                break;
            case TESTS:
                tests(&level, &action);
                break;
        }
    }

    al_destroy_timer(timer);
    al_destroy_display(display);
    al_destroy_event_queue(event_queue);

    return 0;
}