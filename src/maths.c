#include <maths.h>

int circleCenter_x(int x, int r)
{
    return x + r;
}

int circleCenter_y(int y, int r)
{
    return y + r;
}

int is_in_radius(float x, float y, float x0, float y0, float R)
{
	if (	((x - x0)*(x - x0)) + ((y - y0)*(y - y0)) <= (R * R)	)
		return 1;
	else
		return 0;
}

int doCirclesIntersect(float x0, float y0, int R0, float x1, float y1, int R1)
{
	if (
		((R0-R1)*(R0-R1)) <= ((x0-x1)*(x0-x1)) + ((y0-y1)*(y0-y1)) && 
		((x0-x1)*(x0-x1)) + ((y0-y1)*(y0-y1)) <= ((R0+R1)*(R0+R1))
	)
		return 1;
	return 0;
}

bool inCircle(float x, float y, float x0, float y0, float r)
{
    if (	((x - x0)*(x - x0)) + ((y - y0)*(y - y0)) <= (r * r)*1	&&
			((x - x0)*(x - x0)) + ((y - y0)*(y - y0)) > (r * r) * 0.97
	)
		return 1;
	else
		return 0;
}

int randRange(int lower, int upper)
{
    return (rand() % (upper - lower + 1)) + lower;
}

void linearizeVector(float *vx, float *vy)
{	/*		v = i (t.x - e.x) + j(t.y - e.y)
	 * 	v = a * u		//u is the unitary vector for v
	 * 	a = sqrt(vy * vy + vx * vx) // calculate this to determine u 
	 * 	ux = vx/a
	 * 	uy = vy/a
	 * 
	 * */
	float a = sqrt(*vy * *vy + *vx * *vx);
	*vy = *vy / a;
	*vx = *vx / a;
}

float getVectorsAngle(float ux, float uy, float vx, float vy)
{	// Returns angle between vectors u and v in DEGREES
	float angle = acos (	(vx*ux + vy * uy) / (	sqrt(vx*vx + vy*vy) * sqrt(ux*ux + uy*uy)	) );
	angle = angle * 57.2958;
	return angle;
}

float distanceAB(int Ax, int Ay, int Bx, int By)
{
	return sqrt(((Ax-Bx)*(Ax-Bx)) + ((Ay-By)*(Ay-By)));
}