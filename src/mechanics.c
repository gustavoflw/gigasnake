#include <mechanics.h>

int collision(SnakeElement *A, SnakeElement *B)
{
    int collision = 0;
    int R0 = A->radius, R1 = B->radius;
    float cx0 = A->position[0] + R0, cy0 = A->position[1] + R0;
    float cx1 = B->position[0] + R1, cy1 = B->position[1] + R1;
    // float cx0 = A->position[0] + R0 + A->v[0], cy0 = A->position[1] + R0 + A->v[1];
    // float cx1 = B->position[0] + R1 + B->v[0], cy1 = B->position[1] + R1 + B->v[1];

    if (doCirclesIntersect(cx0, cy0, R0, cx1, cy1, R1))
        collision = 1;
    return collision;
}

void eatFood(Snake *snake, SnakeElement *food)
{
    puts("eating");

    SnakeElement *newElement = snakeElementInitialize(snake->first->radius);
    newElement->position[0] = food->position[0];
    newElement->position[1] = food->position[1];
    newElement->color = food->color;

    int n = food->radius * food->radius / (DEFRADIUS * DEFRADIUS);
    printf("food n: %i\n", n);

    food->color = al_map_rgb(randRange(100,255), randRange(100,255), randRange(100,255));
    food->color = al_map_rgb(0,255,0);

    int colorchooser = randRange(0,5);
    switch (colorchooser) {
    case 0:
        food->color = al_map_rgb(0,255,0);
        break;
    case 1:
        food->color = al_map_rgb(255,0,0);
        break;
    case 2:
        food->color = al_map_rgb(0,0,255);
        break;
    case 3:
        food->color = al_map_rgb(255,255,0);
        break;
    case 4:
        food->color = al_map_rgb(255,0,255);
        break;
    case 5:
        food->color = al_map_rgb(0,255,255);
        break;
    }

    food->radius = randRange(DEFRADIUS,45);
    food->position[0] = randRange(0,  SCR_W*7/8);
    food->position[1] = randRange(0,  SCR_H*7/8);

    snakeInsert(snake, n, newElement, newElement->position, &newElement->color);
}