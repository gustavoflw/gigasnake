#include <ouroboros.h>


void ouroborosUpdatePosition(Snake *snake, int Cx, int Cy, int r)
{
    SnakeElement *iterator = snake->last;
    while (iterator != NULL) {
        ouroborosUpdateSpeed(iterator, Cx, Cy, r);
        iterator->position[0] = iterator->position[0] + iterator->v[0];
        iterator->position[1] = iterator->position[1] + iterator->v[1];
        iterator = iterator->before;
    }
}

void ouroborosUpdateSpeed(SnakeElement *element, int Cx, int Cy, int r)
{
    if (element->before == NULL) {

        // Cx = (float)Cx; Cy = (float)Cy; r = (float)r;

        float x0 = element->position[0]+element->radius, y0 = element->position[1]+element->radius;

        if (inCircle(x0, y0, Cx, Cy, r)) {
            // puts("in circle");

            element->v[0] = 0;
            element->v[1] = 0;

            float ux = Cx - x0, uy = Cy - y0;
            linearizeVector(&ux, &uy);
            float multiplier = 1;
            float vx = uy * multiplier, vy = -ux * multiplier;
            // float angle = getVectorsAngle(ux, uy, vx, vy);
            // printf("angle %f\n", angle);

            element->v[0] = vx;
            element->v[1] = vy;
        }
        else {
            if (is_in_radius(x0, y0, Cx, Cy, r)) {
                // puts("inside radius");
            }
            else {
                // puts("outside radius");
                float vx = Cx - x0, vy = Cy - y0;
                linearizeVector(&vx, &vy);
                int multiplier = 1;
                element->v[0] = vx * multiplier;
                element->v[1] = vy * multiplier;
                
            }
        }
    }
    else {
        // float mult = (0.000255)*element->before->radius*element->before->radius;
        float d = distanceAB(element->before->position[0]+element->before->radius, element->before->position[1]+element->before->radius, 
                            element->position[0]+element->radius, element->position[1]+element->radius);
        float mult = 0;
        if (!is_in_radius(
            element->before->position[0]+element->before->radius, element->before->position[1]+element->before->radius, 
            element->position[0]+element->radius, element->position[1]+element->radius, element->before->radius
            )
        )
            mult = element->before->radius*0.14 / d;
        else
            mult = 0.035;
        element->v[0] = mult*(element->before->position[0]+element->before->radius - (element->position[0]+element->radius));
        element->v[1] = mult*(element->before->position[1]+element->before->radius - (element->position[1]+element->radius));
    }
}

// Snake *ouroborosInitialize(int elementR, int nElements, int x, int y)
// {
//     SnakeElement *element = snakeElementInitialize(elementR);
//     element->position[0] = x;
//     element->position[1] = y;
//     Snake *ouroboros = snakeInitialize();
//     ouroboros->type = OUROBOROS;
//     snakeInsert(ouroboros, nElements, element, element->position);
//     return ouroboros;
// }

// void ouroborosUpdatePosition(Snake *snake, int Cx, int Cy, int r)
// {
//     SnakeElement *iterator = snake->last;
//     while (iterator != NULL) {
//         ouroborosElementUpdateSpeed(iterator, Cx, Cy, r);
//         iterator->position[0] = iterator->position[0] + iterator->v[0];
//         iterator->position[1] = iterator->position[1] + iterator->v[1];
//         iterator = iterator->before;
//     }
// }

// void ouroborosElementUpdateSpeed(SnakeElement *element, int Cx, int Cy, int r)
// {
//     if (element->before == NULL) {

//         // Cx = (float)Cx; Cy = (float)Cy; r = (float)r;

//         float x0 = element->position[0]+element->radius, y0 = element->position[1]+element->radius;

//         if (inCircle(x0, y0, Cx, Cy, r)) {
//             // puts("in circle");

//             element->v[0] = 0;
//             element->v[1] = 0;

//             float ux = Cx - x0, uy = Cy - y0;
//             linearizeVector(&ux, &uy);
//             float multiplier = 1;
//             float vx = uy * multiplier, vy = -ux * multiplier;
//             // float angle = getVectorsAngle(ux, uy, vx, vy);
//             // printf("angle %f\n", angle);

//             element->v[0] = vx;
//             element->v[1] = vy;
//         }
//         else {
//             if (is_in_radius(x0, y0, Cx, Cy, r)) {
//                 // puts("inside radius");
//             }
//             else {
//                 // puts("outside radius");
//                 float vx = Cx - x0, vy = Cy - y0;
//                 linearizeVector(&vx, &vy);
//                 int multiplier = 1;
//                 element->v[0] = vx * multiplier;
//                 element->v[1] = vy * multiplier;
                
//             }
//         }
//     }
//     else {
//         element->v[0] = 0.05*(element->before->position[0] - element->position[0]);
//         element->v[1] = 0.05*(element->before->position[1] - element->position[1]);
//     }
// }