#include <playersnake.h>

Snake *playerSnakeInitialize(int x, int y, int elementR, int nElements, ALLEGRO_COLOR color)
{
    SnakeElement *element = snakeElementInitialize(elementR);
    element->position[0] = x;
    element->position[1] = y;
    element->color = color;
    Snake *snake = snakeInitialize();
    snake->px = NULL;
    snake->py = NULL;
    snake->player = 1;
    snakeInsert(snake, nElements, element, element->position, &element->color);
    return snake;
}

void playerSnakeUpdatePosition(bool *keys, Snake *snake, float speed, SnakeElement *B, Hotkey hotkey)
{
    SnakeElement *iterator = snake->last;
    while (iterator != NULL) {
        playerElementUpdateSpeed(keys, iterator, speed, hotkey);
        if (iterator != snake->first) {
            iterator->position[0] = iterator->position[0] + iterator->v[0];
            iterator->position[1] = iterator->position[1] + iterator->v[1];
        }
        else {
            if (collision(iterator, B)) {
                eatFood(snake, B); 
            }
            else {
                iterator->position[0] = iterator->position[0] + iterator->v[0];
                iterator->position[1] = iterator->position[1] + iterator->v[1];
            }
        }
        iterator = iterator->before;
    }
}

int playerElementUpdateSpeed(bool *keys, SnakeElement *element, float speed, Hotkey hotkey)
{
    if (element->before == NULL) { // dealing with first element
        if (keys[hotkey.up] && !keys[hotkey.right] && !keys[hotkey.down] && !keys[hotkey.left]) {
            element->v[1] = -speed;
            element->v[0] = 0;
        }
        if (keys[hotkey.down] && !keys[hotkey.right] && !keys[hotkey.up] && !keys[hotkey.left]) {
            element->v[1] = speed;
            element->v[0] = 0;
        }
        if (keys[hotkey.right] && !keys[hotkey.left] && !keys[hotkey.down] && !keys[hotkey.up]) {
            element->v[0] = speed;
            element->v[1] = 0;
        }
        if (keys[hotkey.left] && !keys[hotkey.down] && !keys[hotkey.right] && !keys[hotkey.up]) {
            element->v[0] = -speed;
            element->v[1] = 0;
        }
    }
    else {
        element->v[0] = 0.1*(element->before->position[0] - element->position[0]);
        element->v[1] = 0.1*(element->before->position[1] - element->position[1]);
    }

    return 1;
}