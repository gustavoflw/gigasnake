#include <shaders.h>

void startShaders(ALLEGRO_SHADER *shader)
{
    if (!shader) {
        puts("shader failed");
    }
    if (!al_attach_shader_source_file(shader, ALLEGRO_VERTEX_SHADER, "resources/vertex.glsl")) {
        puts("vertex attach failed");
    }
    if (!al_attach_shader_source_file(shader, ALLEGRO_PIXEL_SHADER, "resources/frag.glsl")) {
        puts("frag failed");
        puts(al_get_shader_log(shader));
    }
    if (!al_build_shader(shader)) {
        puts("build shader failed");
        puts(al_get_shader_log(shader));
    }
}

void drawRadientShader(float radius, int Cx, int Cy, float expand, float alpha, ALLEGRO_COLOR color)
{
    float col[] = {color.r, color.g, color.b, alpha};
    float cent[] = {(float)Cx, (float)Cy};
    
    al_set_shader_float_vector("color", 4, col, 1);
    al_set_shader_float_vector("center", 2, cent, 1);
    al_set_shader_float("radius", radius);
    al_set_shader_float("expand", expand);
    al_set_shader_float("windowHeight", 600);
    al_draw_filled_circle(cent[0], cent[1], radius, al_map_rgba(0,0,0,0));
}