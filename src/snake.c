#include <snake.h>

Snake *snakeInitialize()
{
    Snake *snake = (Snake *) malloc(sizeof(Snake));
    snake->first = NULL;
    snake->last = NULL;
    snake->px = NULL;
    snake->py = NULL;
    snake->player = 0;
    if (snake == NULL)
        return 0;
    return snake;
}

SnakeElement *snakeElementInitialize(float radius)
{
    SnakeElement *element = (SnakeElement *) malloc(sizeof(SnakeElement));
    element->next = NULL;
    element->before = NULL;
    element->position = (float *) malloc(2*sizeof(float));
    element->position[0] = -40;
    element->position[1] = -40;
    element->v = (float *) malloc(2*sizeof(float));
    element->v[0] = 0;
    element->v[1] = 0;
    element->radius = radius;
    element->color = al_map_rgb(255,255,255);

    if (element == NULL || element->position == NULL || element->v == NULL)
        return 0; 
    return element;
}

int snakeLength(Snake *snake)
{
    SnakeElement *iterator = snake->first;
    int length = 0;
    while (iterator != NULL) {
        iterator = iterator->next;
        length++;
    }
    return length;
}

int snakeInsert(Snake *snake, int n, SnakeElement *element, float *elementStartPosition, ALLEGRO_COLOR *color)
{
    SnakeElement *newElement = NULL;
    if (n != 0) {
        if (snakeLength(snake) == 0) {
            newElement = element;
            newElement->v[0] = 1;
            newElement->v[1] = 0;
            snake->first = newElement;
            snake->last = newElement;
        }
        else if (snakeLength(snake) == 1) {
            newElement = element;
            snake->first->next = newElement;
            snake->last = newElement;
            snake->last->before = snake->first;
        }
        else {
            newElement = element;
            snake->last->next = newElement;
            newElement->before = snake->last;
            snake->last = newElement;
        }

        if (elementStartPosition != NULL) {
            newElement->position[0] = elementStartPosition[0];
            newElement->position[1] = elementStartPosition[1];
        }
        if (color != NULL) {
            newElement->color = *color;
        }
    }
    else {
        snakeElementClear(element); //
        return 0;
    }
    
    snakeInsert(snake, n-1, snakeElementInitialize(element->radius), elementStartPosition, color);    
    
    if (newElement == NULL)
        return 0;
    return 1;
}

void snakeElementClear(SnakeElement *element)
{
    free(element->position);
    if (element->position)
        element->position = NULL;
    
    free(element->v);
    if (element->v)
        element->v = NULL;
    
    free(element);
    if (element)
        element = NULL;
}

void snakeClear(Snake *snake)
{
    puts("clearing snake...");
    if (snake->first) {
        SnakeElement* iterator = snake->first;
        SnakeElement* aux = NULL;
        while (iterator != NULL) {
            aux = iterator->next;
            snakeElementClear(iterator);
            iterator = aux;
        }
    }
    snake->first = NULL;
    snake->last = NULL;
    snake->px = NULL;
    snake->py = NULL;
    liberarPilha(snake->px);
    liberarPilha(snake->py);
    free(snake);
    snake = NULL;
}

void drawSnake(Snake *snake)
{
    SnakeElement *iterator = snake->last;
    while (iterator != NULL) {
        drawSnakeElement(iterator);
        iterator = iterator->before; 
    }
}

void drawSnakeElement(SnakeElement *element)
{
    float R = element->radius;
    float cx = circleCenter_x(element->position[0], R);
    float cy = circleCenter_y(element->position[1], R);

    al_draw_filled_circle(cx, cy, R, element->color);
    // al_draw_circle(cx, cy, R, al_map_rgb(100,100,100), 1);
}





