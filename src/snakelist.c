#include <snakelist.h>

SnakeList *snakeListInitialize()
{
    SnakeList *snakeList = (SnakeList *) malloc(sizeof(SnakeList));
    snakeList->first = NULL;
    return snakeList;
}

SnakeListElement *snakeListElementInitialize()
{
    SnakeListElement *listElement = (SnakeListElement *)malloc(sizeof(SnakeListElement));
    listElement->next = NULL;
    listElement->snake = NULL;
    return listElement;
}

int snakeListLength(SnakeList *snakeList)
{
    SnakeListElement *iterator = snakeList->first;
    int length = 0;
    while (iterator != NULL) {
        iterator = iterator->next;
        length++;
    }
    return length;
}

void snakeListInsert(SnakeList *snakeList, Snake *snake)
{
    SnakeListElement *newElement = snakeListElementInitialize();
    newElement->snake = snake;

    if (snakeListLength(snakeList) == 0) {
        snakeList->first = newElement;
    }
    else {
        SnakeListElement *iterator = snakeList->first;
        while (iterator->next != NULL) {
            iterator = iterator->next;
        }
        iterator->next = newElement;
    }
}

void snakeListElementClear(SnakeListElement* iterator)
{
    snakeClear(iterator->snake);
    free(iterator);
}

void snakeListClear(SnakeList* snakeList)
{
    SnakeListElement* iterator = snakeList->first;
    SnakeListElement* aux = NULL;
    while (iterator != NULL) {
        aux = iterator->next;
        snakeListElementClear(iterator);
        iterator = aux;
    }
    free(snakeList);
}

