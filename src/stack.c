#include <stack.h>

Pilha *inicializaPilha() 
{
    Pilha *p;
    p = (Pilha *)malloc(sizeof(Pilha));
	p->topo = NULL;
    return p;
}

void Push(Pilha *p, int d) 
{
    ElementoPilha *novo = (ElementoPilha *)malloc(sizeof(ElementoPilha));
    novo->dado = d;
    novo->proximo = p->topo;
    p->topo = novo;
}

int Pop(Pilha *p) 
{
	int saida = 0;
    if (p != NULL || p->topo != NULL)
	{
        saida = p->topo->dado;
        ElementoPilha *aux = p->topo;
        p->topo = p->topo->proximo;
        free(aux);
        aux = NULL;
    }
	return saida;
}

void imprimePilha(Pilha *p) 
{
    ElementoPilha *iterador;    
    iterador = p->topo;
    while (iterador != NULL) {
        printf("%i ",iterador->dado);
        iterador = iterador->proximo;
    }
	printf("\n");
}

void liberarPilha (Pilha *p)
{
    if (p != NULL)
    {
        while (p->topo != NULL) 
            Pop(p);
        free(p);
    }
}